package katas.fizzbuzz;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FizzBuzzShould {


    @Test
    public void return_normal_number() {
        //given
        int number = 1;
        String shouldNumber = "1";
        testTransformedNumber(number, shouldNumber);
    }

    private void testTransformedNumber(int number, String shouldNumber) {
        //when
        katas.fizzbuzz.FizzBuzz fizzBuzz = new katas.fizzbuzz.FizzBuzz();
        String transformedNumber = fizzBuzz.transform(number);
        //then
        assertThat(transformedNumber, is(shouldNumber));
    }

    @Test
    public void return_fizz_number() {
        int number = 3;
        testTransformedNumber(number, "fizz");
    }

    @Test
    public void return_buzz_number() {
        int number = 5;
        testTransformedNumber(number, "buzz");
    }

    @Test
    public void return_fizzbuzz_number() {
        int number = 15;
        testTransformedNumber(number, "fizzbuzz");
    }


    @Test
    public void return_pop_number() {
        int number = 7;
        testTransformedNumber(number, "pop");
    }

    @Test
    public void return_fuzz_number() {
        int number = 2;
        testTransformedNumber(number, "fuzz");
    }

    @Test
    public void return_popfuzz_number() {
        int number = 14;
        testTransformedNumber(number, "popfuzz");
    }

    @Test
    public void return_fizzpop_number() {
        int number = 21;
        testTransformedNumber(number, "fizzpop");
    }

    @Test
    public void return_fizzpopfuzz_number() {
        int number = 42;
        testTransformedNumber(number, "fizzpopfuzz");
    }

    @Test
    public void return_fizzfuzz_number() {
        int number = 6;
        testTransformedNumber(number, "fizzfuzz");
    }

    @Test
    public void return_fizzbuzzpop_number() {
        int number = 105;
        testTransformedNumber(number, "fizzbuzzpop");
    }

    @Test
    public void return_fizzbuzzpopfuzz_number() {
        int number = 210;
        testTransformedNumber(number, "fizzbuzzpopfuzz");
    }

    @Test
    public void return_buzzpop_number() {
        int number = 35;
        testTransformedNumber(number, "buzzpop");
    }

    @Test
    public void return_buzzpopfuzz_number() {
        int number = 70;
        testTransformedNumber(number, "buzzpopfuzz");
    }


}
