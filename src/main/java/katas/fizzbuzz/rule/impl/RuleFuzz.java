package katas.fizzbuzz.rule.impl;

import katas.fizzbuzz.rule.Rule;

public class RuleFuzz implements Rule {

    @Override
    public String apply(int number) {
        return number % 2 == 0 ? "fuzz" : "";
    }
}
