package katas.fizzbuzz.rule.impl;

import katas.fizzbuzz.rule.Rule;

public class RuleBuzz implements Rule {

    @Override
    public String apply(int number) {
        return number % 5 == 0 ? "buzz" : "";
    }

}
