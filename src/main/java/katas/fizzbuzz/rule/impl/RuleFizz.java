package katas.fizzbuzz.rule.impl;

import katas.fizzbuzz.rule.Rule;

public class RuleFizz implements Rule {

    @Override
    public String apply(int number) {
        return number % 3 == 0 ? "fizz" : "";
    }
}
