package katas.fizzbuzz.rule.impl;

import katas.fizzbuzz.rule.Rule;

public class RulePop implements Rule {


    @Override
    public String apply(int number) {
        return number % 7 == 0 ? "pop" : "";
    }
}
