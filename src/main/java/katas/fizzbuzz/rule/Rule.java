package katas.fizzbuzz.rule;

public interface Rule {

    String apply(int number);

}
