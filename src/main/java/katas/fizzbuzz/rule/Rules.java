package katas.fizzbuzz.rule;

import katas.fizzbuzz.rule.impl.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Rules implements Iterable<Rule>, Iterator<Rule> {

    private List<Rule> rules;
    private int count = 0;

    public Rules() {
        this.rules = new ArrayList<>();
        this.rules.add(new RuleFizz());
        this.rules.add(new RuleBuzz());
        this.rules.add(new RulePop());
        this.rules.add(new RuleFuzz());
    }

    @Override
    public Iterator<Rule> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return this.count < this.rules.size();
    }

    @Override
    public Rule next() {
        if (this.count == this.rules.size()) {
            throw new NoSuchElementException();
        }
        this.count++;
        return this.rules.get(this.count - 1);
    }
}
