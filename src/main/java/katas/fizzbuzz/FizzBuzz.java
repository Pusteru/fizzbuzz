package katas.fizzbuzz;

import katas.fizzbuzz.rule.Rule;
import katas.fizzbuzz.rule.Rules;


class FizzBuzz {

    String transform(int number) {

        StringBuilder transformedNumber = new StringBuilder();
        Rules rules = new Rules();

        for (Rule rule : rules.getRules()) {
            transformedNumber.append(rule.transformedName(number));
        }

        return transformedNumber.toString();
    }
}
